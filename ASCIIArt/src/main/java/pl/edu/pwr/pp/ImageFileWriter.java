package pl.edu.pwr.pp;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

public class ImageFileWriter 
{

	public void saveToTxtFile(char[][] ascii, String fileName) 
	{	
		try(  PrintWriter out = new PrintWriter(fileName))
		{
			for(char [] i: ascii)
		    out.println(i);
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
