package pl.edu.pwr.pp;

public class ImageConverter 
{

	public static String INTENSITY_2_ASCII = "@%#*+=-:. ";
	private static int Range = 256;


	public static char intensityToAscii(int intensity) 
	{
		return INTENSITY_2_ASCII.charAt(intensity * INTENSITY_2_ASCII.length() / Range);
	}

	public static char[][] intensitiesToAscii(int[][] intensities) 
	{
		char[][] tableOfAsciiChar = new char[intensities.length][intensities[0].length];
		for(int i = 0; i < intensities.length; i++)
		{
			for(int j = 0; j < intensities[i].length; j++)
				tableOfAsciiChar[i][j] = intensityToAscii(intensities[i][j]); 
		}
		return tableOfAsciiChar;
	}

}
