package pl.edu.pwr.pp;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
public class ImageFileReader 
{

	public int[][] readPgmFile(String fileName) throws Exception 
	{
		int columns = 0;
		int rows = 0;
		int[][] intensities = null;

		Path path = this.getPathToFile(fileName);
		
		try (BufferedReader reader = Files.newBufferedReader(path)) 
		{
			validateFileType(reader);
			
			String rowAndColumn  = reader.readLine();
			String [] rowAndColumnTable = rowAndColumn.split(" ");
			columns = Integer.parseInt(rowAndColumnTable[0]);
			rows = Integer.parseInt(rowAndColumnTable[1]);
			System.out.println(columns);
			
			System.out.println(rows);
			reader.readLine();
			
			intensities = readPixelsIntensities(reader, columns, rows);
			
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		return intensities;
	}
	
	private int[][] readPixelsIntensities(BufferedReader reader, int columns, int rows) throws NumberFormatException, IOException {
		int[][] intensities = null;
		intensities = new int[rows][];
		for (int i = 0; i < rows; i++) 
			intensities[i] = new int[columns];

		String line = null;
		int currentRow = 0;
		int currentColumn = 0;
		while ((line = reader.readLine()) != null) 
		{
			String[] elements = line.split(" ");
			for (int i = 0; i < elements.length; i++) 
			{
				intensities[currentRow][currentColumn] = Integer.parseInt(elements[i]);
					currentColumn++;
					if(currentColumn == columns)
					{	
						currentColumn = 0;
						currentRow++;
					}
			}
		}
		return intensities;
	}

	private void validateFileType(BufferedReader reader) throws Exception {
		
		String P2 = reader.readLine();
		if(!P2.equals("P2"))
			throw new Exception("Wrong file format");
		
		reader.readLine();
		
	}

	private Path getPathToFile(String fileName) throws URISyntaxException 
	{
		URI uri = ClassLoader.getSystemResource(fileName).toURI();
		return Paths.get(uri);
	}
	
}
